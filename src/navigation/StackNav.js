import React from 'react'
import { Button, Image } from 'react-native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import App from '../../App'
import Blue from '../views/Blue'
import Yellow from '../views/Yellow'

const Stack = createNativeStackNavigator();

const StackNav = () => {
    return (
        <Stack.Navigator 
            initialRouteName={"Blue"}
            screenOptions={{
                headerStyle: {
                    backgroundColor: 'darkblue',
                },
                headerTintColor: 'white',
                headerTitleStyle: {
                    fontWeight: 'bold',
                },
            }}>
            <Stack.Screen name="Camera" component={App}></Stack.Screen>
            <Stack.Screen 
                name="Blue" 
                component={Blue} 
                options={{
                    title:'Biru',
                    headerTitle:(props)=><Image style={{height:50, width:100, resizeMode:'contain'}} source={{uri:'https://coding.id/images/coding_id_logo.png'}}></Image>,
                    headerRight:(props)=><Button title='Hello'></Button>,
                    headerLeft:(props)=><Button title='back'></Button>,
                    headerTitleAlign:'center'
                    
                }}></Stack.Screen>
            <Stack.Screen 
                name="Yellow" 
                component={Yellow} 
                options={{
                    title:'Kuning', 
                    headerStyle: {
                        backgroundColor: '#f4511e',
                    },
                    headerTintColor: '#fff',
                    headerTitleStyle: {
                        fontWeight: 'bold',
                    },

                }}
                ></Stack.Screen>
        </Stack.Navigator>
    )
}

export default StackNav
