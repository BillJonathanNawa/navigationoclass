import React from 'react'
import { View, Text } from 'react-native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons'

import App from '../../App'
import Blue from '../views/Blue'
import Yellow from '../views/Yellow'

import StackBaru from './StackBaru'

const Tab = createBottomTabNavigator();

const TabNav = () => {
    return (
        <Tab.Navigator screenOptions={({route})=>({
            tabBarIcon:({ focused, color, size }) => {
                let iconName;
    
                if (route.name === 'StackBaru') {
                  iconName = "battery-full-outline"
                } else if (route.name === 'Camera') {
                  iconName = "camera-outline"
                } else if (route.name === 'Yellow') {
                    iconName = "flash-outline"
                  }
    
                // You can return any component that you like here!
                return <Ionicons name={iconName} size={size} color={color} />;
            },
            tabBarActiveTintColor: 'black',
            tabBarInactiveTintColor: 'yellow',
            headerShown:false,
            headerStyle: {
                backgroundColor: 'darkblue',
            },
            headerTintColor: 'white',
            headerTitleStyle: {
                fontWeight: 'bold',
            }
        })}>
            <Tab.Screen name="Blue" component={Blue} options={{headerShown:false}} />
            <Tab.Screen name="Camera" component={App} />
            <Tab.Screen name="Yellow" component={Yellow} />
        </Tab.Navigator>
    )
}

export default TabNav

