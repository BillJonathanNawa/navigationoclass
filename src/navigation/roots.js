import React from 'react'
import { NavigationContainer } from '@react-navigation/native';

import StackNav from './StackNav'
import TabNav from './TabNav'
import StackBaru from './StackBaru'
const roots = () => {
    return (
        <NavigationContainer>
            {/* <StackNav/> */}
            {/* <TabNav/> */}
            <StackBaru/>
        </NavigationContainer>
    )
}

export default roots
