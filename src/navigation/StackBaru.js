import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Ionicons from 'react-native-vector-icons/Ionicons'

import Red from '../views/Red'
import Blue from '../views/Blue'
import Green from '../views/Green'
import TabNav from '../navigation/TabNav'
const Stack = createNativeStackNavigator();

const StackBaru = () => {
    return (
        <Stack.Navigator screenOptions={{
            headerRight:(props)=><Ionicons name="notifications-outline"></Ionicons>
        }}>
            <Stack.Screen name="TabNav" component={TabNav}></Stack.Screen>
            <Stack.Screen name="Red" component={Red}></Stack.Screen>
            <Stack.Screen name="Green" component={Green}></Stack.Screen>
        </Stack.Navigator>
    )
}

export default StackBaru
