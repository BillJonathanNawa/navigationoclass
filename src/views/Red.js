import React from 'react'
import { View, Text, Button } from 'react-native'

const Red = ({navigation}) => {
    
    React.useEffect(()=>{
        const update = navigation.addListener('focus', () => {
          // Screen was focused
          // Do something
          
          console.log("Red")
        });
    
        return update;
      },[navigation])
    return (
        <View>
            <Text>RED</Text>
            <Button title="go to green" onPress={()=>navigation.navigate('Green')}></Button>
        </View>
    )
}

export default Red
