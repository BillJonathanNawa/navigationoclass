import React from 'react'
import { View, Text, Button } from 'react-native'

const Yellow = ({route, navigation}) => {
    // const dataUser = route.params

    React.useEffect(()=>{
        console.log("Yellow")
    },[])
    return (
        <View style={{flex:1, backgroundColor:'yellow'}}>
            <Button
                title="Back"
                onPress={()=>navigation.navigate("Blue")}
            ></Button>
            {/* <Text>{dataUser.name}</Text>
            <Text>{dataUser.address}</Text>
            <Text>{dataUser.phoneNumber}</Text> */}
        </View>
    )
}

export default Yellow
