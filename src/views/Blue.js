import React, {useState} from 'react'
import { View, Text, Button } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons'
import FontAwesome from 'react-native-vector-icons/FontAwesome5'

const Blue = (props) => {
    const {navigation} = props

    const [dataUser, setDataUser] = useState({
        name:"Bill",
        address:"Jakbar",
        phoneNumber:"081234567890"
    })

    React.useEffect(()=>{
        console.log("Blue")
    },[])
    return (
        <View style={{flex:1, backgroundColor:'blue'}}>
            <Ionicons name="airplane-outline" size={30} color="white"/>
            <FontAwesome name="plane" size={30} color="white"/>
            <Button
                title="Next Page"
                onPress={()=>navigation.navigate("Red",{dataDariBlue:dataUser, judul:"hello World"})}
            ></Button>
        </View>
    )
}

export default Blue

