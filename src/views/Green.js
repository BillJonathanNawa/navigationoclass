import React from 'react'
import { View, Text } from 'react-native'

const Green = () => {
    
    React.useEffect(()=>{
        console.log("Green")

        return () => console.log("Green Out")
    },[])
    
    return (
        <View>
            <Text>GREEN</Text>
        </View>
    )
}

export default Green
