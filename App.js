import React, {useState} from 'react'
import { View, Text, Pressable, StyleSheet, Image, Button } from 'react-native'
import {launchCamera, launchImageLibrary} from 'react-native-image-picker'


const App = ({route, navigation}) => {
  const [image1, setimage1] = useState({})
  const [image2, setimage2] = useState({})

  React.useEffect(()=>{
    const update = navigation.addListener('focus', () => {
      // Screen was focused
      // Do something
      
      console.log("Camera")
    });

    return update;
  },[navigation])
  // const {dataDariBlue, judul} = route.params

  // route.params = {
  //      dataDariBlue:dataUser, 
  //      judul:"hello World"
  // }
  // const hello = route.params
  // React.useEffect(()=>{
  //   console.log(dataDariBlue)
  //   console.log(judul)
  // },[])

  const onCamera = () => {
    let options = {
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    launchCamera(options, (response) => {
      console.log('Response = ', response);
    
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = response.assets[0];
        setimage1({
         filePath: source.fileName,
         fileData: source.base64,
         fileUri: source.uri
        });
      }
    })
  }

  const onGallery = ()=>{
    let options = {
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    
    launchImageLibrary(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        alert(response.customButton);
      } else {
        const source = response.assets[0];
        console.log('response', JSON.stringify(response));
        setimage2({
          filePath: source.fileName,
          fileData: source.base64,
          fileUri: source.uri
        });
      }
    });
  }

  return (
    <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
      <Text>Camera</Text>
      <View style={{flexDirection:'row'}}>
        <View style={styles.image}>
          <Image style={{height:'100%', width:'100%'}} source={{uri:image1.fileUri||null}}/>
        </View>
        <View style={styles.image}>
          <Image style={{height:'100%', width:'100%'}} source={{uri:image2.fileUri||null}}/>
        </View>
      </View>
      <Pressable onPress={onCamera} style={styles.button}>
        <Text>Take a Picture</Text>
      </Pressable>
      <Pressable onPress={onGallery} style={styles.button}>
        <Text>Take from gallery</Text>
      </Pressable>
      <Button
          title="Next Page"
          onPress={()=>navigation.navigate("Yellow")}
      ></Button>

      {/* <Text>{judul}</Text>
      <Text>{dataDariBlue.name}</Text>
      <Text>{dataDariBlue.address}</Text>
      <Text>{dataDariBlue.phoneNumber}</Text> */}
      {/* <Text>{hello}</Text> */}
    </View>
  )
}

const styles = StyleSheet.create({
  button:{
    width: 200,
    margin:5,
    padding:10,
    backgroundColor:'skyblue',
    borderRadius:5
  },
  image:{
    width:100,
    height:100,
    borderRadius:5,
    margin: 5,
    backgroundColor:'lightgrey'
  }
})

export default App
