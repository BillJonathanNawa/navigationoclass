/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import Roots from './src/navigation/roots'
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => Roots);
